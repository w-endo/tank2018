﻿#pragma once
#include "Engine\Fbx.h"
#include "Engine\Camera.h"


class Tank : public  MyGameEngine::Fbx
{
	MyGameEngine::Camera* _pCamera;

public:
	Tank();
	~Tank();
	HRESULT Init() override;
	BOOL Update()	override;
};

