﻿#include "Tank.h"
#include "Engine\Camera.h"
#include "Engine\Scene.h"
using namespace MyGameEngine;


Tank::Tank()
{
	_pCamera = NULL;
}


Tank::~Tank()
{
}

HRESULT Tank::Init()
{
	_pCamera = new MyGameEngine::Camera;
	_parent->AddChild(_pCamera);


	return Fbx::Init("Models/Tank.fbx");
}

BOOL Tank::Update()
{
	D3DXMATRIX mat;
	D3DXMatrixRotationY(&mat, D3DXToRadian(_rotate.y));

	if (g.pInput->IsKeyPush(DIK_LEFT))
	{
		_rotate.y -= 1;
	}
	if (g.pInput->IsKeyPush(DIK_RIGHT))
	{
		_rotate.y += 1;
	}

	if (g.pInput->IsKeyPush(DIK_UP))
	{
		D3DXVECTOR3 move = D3DXVECTOR3(0, 0, 0.1f);



		D3DXVec3TransformCoord(&move, &move, &mat);

		_position += move;
	}


	D3DXVECTOR3 cameraArm = D3DXVECTOR3(0, 6, -10);
	D3DXVec3TransformCoord(&cameraArm, &cameraArm, &mat);
	_pCamera->SetPosition(_position + cameraArm);
	_pCamera->SetTarget(_position);


	return FALSE;
}
