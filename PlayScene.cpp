﻿#include "PlayScene.h"
#include "Engine\Light.h"
#include "Tank.h"
#include "Ground.h"

PlayScene::PlayScene()
{
}


PlayScene::~PlayScene()
{
}

HRESULT PlayScene::Init()
{
	//各オブジェクトの準備
	AddChild(new MyGameEngine::Light);
	//AddChild(new MyGameEngine::Camera);
	AddChild(new Tank);
	AddChild(new Ground);

	return S_OK;

}

