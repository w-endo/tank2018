﻿#pragma once
#include <fbxsdk.h>
#include "Node.h"

#pragma comment(lib,"libfbxsdk-mt.lib")

namespace MyGameEngine
{
	class Fbx : public Node
	{
		//ポリゴンの1頂点に必要なデータをまとめた構造体
		struct Vertex
		{
			D3DXVECTOR3 pos;	//位置
			D3DXVECTOR3 normal;	//法線
			D3DXVECTOR2 uv;		//UV座標
		};


		FbxManager*  _pManager;
		FbxImporter* _pImporter;
		FbxScene*    _pScene;

		int _vertexCount;
		int _polygonCount;
		int _indexCount;
		int _materialCount;
		int* _polygonCountOfMaterial;

		LPDIRECT3DVERTEXBUFFER9 _pVertexBuffer;	//頂点バッファ
		LPDIRECT3DINDEXBUFFER9*	_pIndexBuffer;	//インデックスバッファ
		LPDIRECT3DTEXTURE9*		_pTexture;		//テクスチャ
		D3DMATERIAL9*			_material;		//マテリアル


		void CheckNode(FbxNode* pNode);
		void CheckMesh(FbxMesh* pMesh);


	public:
		//コンストラクタ
		Fbx();

		//デストラクタ
		~Fbx();

		//機能：初期化
		//引数：なし
		//戻値：成功したか
		HRESULT Init(LPCSTR fileName);

		//機能：描画処理
		//引数：なし
		//戻値：なし
		void Draw();


		//各セッター
		void SetPosition(D3DXVECTOR3 position);
		void SetPosition(float x, float y, float z);
		void SetRotate(D3DXVECTOR3 rotate);
		void SetRotate(float x, float y, float z);
		void SetScale(D3DXVECTOR3 scale);
		void SetScale(float x, float y, float z);

		//各ゲッター
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetRotate();
		D3DXVECTOR3 GetScale();

	};
}
