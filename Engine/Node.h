﻿#pragma once
#include "Global.h"
namespace MyGameEngine
{
	class Fbx;

	class Node
	{
	protected:
		D3DXVECTOR3 _position;	//位置
		D3DXVECTOR3 _rotate;	//回転角度
		D3DXVECTOR3 _scale;		//拡大率

		Scene*		_parent;	//親シーン

		BOOL		_isDead;

	public:
		Node();
		~Node();


		virtual HRESULT Init() { return S_OK; }

		//更新処理
		//引数：なし
		//戻値：シーンが切り替わったらTRUE
		virtual BOOL Update() { return FALSE; }



		virtual void Draw() = 0;

		virtual void Hit(Fbx* pTarget) {};


		void SetParent(Scene* parent) { _parent = parent; }
		Scene* GetParent() { return _parent; }

		BOOL	IsDead() { return _isDead; }
		void	Kill(BOOL isDead) { _isDead = isDead; }

	};
}

