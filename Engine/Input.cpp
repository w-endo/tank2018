﻿#include "Input.h"
using namespace MyGameEngine;

Input::Input()
{
	_pDInput = NULL;
	_pKeyDevice = NULL;
}


Input::~Input()
{
	ZeroMemory(_keyState, sizeof(_keyState));
	ZeroMemory(_keyState, sizeof(_prevKeyState));
	SAFE_RELEASE(_pKeyDevice);
	SAFE_RELEASE(_pDInput);
}

void Input::Init(HWND hWnd)
{
	DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&_pDInput, NULL);

	_pDInput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, NULL);
	_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
	_pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
}

void Input::Update()
{
	_pKeyDevice->Acquire();
	memcpy(_prevKeyState, _keyState, sizeof(_keyState));
	_pKeyDevice->GetDeviceState(sizeof(_keyState), &_keyState);
}

BOOL Input::IsKeyPush(int keyCode)
{
	if (_keyState[keyCode] & 0x80)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL Input::IsKeyTap(int keyCode)
{
	if (IsKeyPush(keyCode) && !(_prevKeyState[keyCode] & 0x80))
	{
		return TRUE;
	}
	return FALSE;
}

BOOL Input::IsKeyRelease(int keyCode)
{
	if (!IsKeyPush(keyCode) && _prevKeyState[keyCode] & 0x80)
	{
		return TRUE;
	}
	return FALSE;
}