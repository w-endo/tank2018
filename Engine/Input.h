﻿#pragma once

#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != NULL){ p->Release(); p = NULL;} 

namespace MyGameEngine
{
	class Input
	{
		LPDIRECTINPUT8 _pDInput;
		LPDIRECTINPUTDEVICE8 _pKeyDevice;
		BYTE _keyState[256];
		BYTE _prevKeyState[256];

	public:
		Input();
		~Input();
		void Init(HWND hWnd);
		void Update();
		BOOL IsKeyPush(int keyCode);
		BOOL IsKeyTap(int keyCode);
		BOOL IsKeyRelease(int keyCode);
	};
}

