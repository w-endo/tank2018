﻿#pragma once
#include <vector>
#include "Global.h"
#include "Node.h"
namespace MyGameEngine
{
	class Scene
	{
	protected:
		std::vector<MyGameEngine::Node*> _pNodes;
	public:
		Scene();
		~Scene();
		virtual HRESULT Init() = 0;
		virtual HRESULT InitEach();

		virtual void Update() {};
		virtual void UpdateEach();

		virtual void Draw();

		void AddChild(Node* node);
	};
}

