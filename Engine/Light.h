﻿//////////////////////////////////////////////////
//　ライトを管理するクラス
//　制作日：2018/6/5
//////////////////////////////////////////////////
#pragma once
#include "Node.h"

namespace MyGameEngine
{
	class Light : public Node
	{
	public:
		//コンストラクタ
		Light();

		//デストラクタ
		~Light();

		void Draw() {}
	};
}