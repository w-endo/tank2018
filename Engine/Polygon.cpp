﻿#include "Polygon.h"
using namespace MyGameEngine;

//コンストラクタ
Polygon::Polygon()
{
	_pVertexBuffer = NULL;
	_pIndexBuffer = NULL;
	_pTexture = NULL;

	ZeroMemory(&_material, sizeof(D3DMATERIAL9));
}

//デストラクタ
Polygon::~Polygon()
{
	SAFE_RELEASE(_pTexture);
	SAFE_RELEASE(_pIndexBuffer);
	SAFE_RELEASE(_pVertexBuffer);
}

//初期化
HRESULT Polygon::Init(LPCSTR fileName)
{
	//頂点情報を設定する
	SetupVertex();

	//インデックス情報を設定する
	SetupIndex();

	//見た目（テクスチャやマテリアル）を設定する
	if (FAILED(SetupMaterial(fileName)))
	{
		return E_FAIL;
	}

	return S_OK;

}

//頂点情報の準備
void Polygon::SetupVertex()
{
	//頂点情報
	Vertex vertexList[] = {
		D3DXVECTOR3(-1, 1, 0),D3DXVECTOR3(0, 0, -1), D3DXCOLOR(255,255,255,255),  D3DXVECTOR2(0, 0),
		D3DXVECTOR3(1, 1, 0),D3DXVECTOR3(0, 0, -1), D3DXCOLOR(255,255,255,255),  D3DXVECTOR2(1, 0),
		D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0, 0, -1),D3DXCOLOR(255,255,255,255),  D3DXVECTOR2(1, 1),
		D3DXVECTOR3(-1, -1, 0),D3DXVECTOR3(0, 0, -1), D3DXCOLOR(255,255,255,255),  D3DXVECTOR2(0, 1),
	};

	//頂点バッファ作成
	g.pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1, D3DPOOL_MANAGED, &_pVertexBuffer, 0);

	//バッファに情報を入れる
	Vertex *vCopy;
	_pVertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(vertexList));
	_pVertexBuffer->Unlock();
}

//インデックス情報の準備
void Polygon::SetupIndex()
{
	//インデックス情報
	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	//インデックスバッファ作成
	g.pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &_pIndexBuffer, 0);

	//バッファに情報を入れる
	DWORD *iCopy;
	_pIndexBuffer->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	_pIndexBuffer->Unlock();
}

//機能：見た目の準備
//引数：テクスチャのファイル名
HRESULT Polygon::SetupMaterial( LPCSTR fileName)
{
	//テクスチャオブジェクトの作成
	if (FAILED(D3DXCreateTextureFromFileEx(g.pDevice, fileName, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture)))
	{
		//失敗時の処理
		char mes[256];
		wsprintf(mes, "ファイル「%s」が開けませんでした", fileName);
		MessageBox(NULL, mes, "エラー", MB_OK);
		return E_FAIL;
	}

	//拡散反射光（色）
	_material.Diffuse.r = 1.0f;
	_material.Diffuse.g = 1.0f;
	_material.Diffuse.b = 1.0f;

	//環境光（影の部分の明かり）
	_material.Ambient.r = 0.2f;
	_material.Ambient.g = 0.2f;
	_material.Ambient.b = 0.2f;
	return S_OK;
}

//描画処理
void Polygon::Draw()
{
	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;

	//変換行列をセット
	g.pDevice->SetTransform(D3DTS_WORLD, &world);

	//頂点とインデックスバッファ
	g.pDevice->SetStreamSource(0, _pVertexBuffer, 0, sizeof(Vertex));
	g.pDevice->SetIndices(_pIndexBuffer);

	//頂点情報の種類
	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);

	//テクスチャとマテリアル
	g.pDevice->SetTexture(0, _pTexture);
	g.pDevice->SetMaterial(&_material);

	//描画
	g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

}


//////////////////////以下、アクセス関数////////////////////////////
void Polygon::SetPosition(D3DXVECTOR3 position)
{
	_position = position;
}

void Polygon::SetPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;
}



void Polygon::SetRotate(D3DXVECTOR3 rotate)
{
	_rotate = rotate;
}

void Polygon::SetRotate(float x, float y, float z)
{
	_rotate.x = x;
	_rotate.y = y;
	_rotate.z = z;
}



void Polygon::SetScale(D3DXVECTOR3 scale)
{
	_scale = scale;
}


void Polygon::SetScale(float x, float y, float z)
{
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;
}



//位置を取得
D3DXVECTOR3 Polygon::GetPosition()
{
	return _position;
}

//回転角度を取得
D3DXVECTOR3 Polygon::GetRotate()
{
	return _rotate;
}

//拡大率を取得
D3DXVECTOR3 Polygon::GetScale()
{
	return _scale;
}